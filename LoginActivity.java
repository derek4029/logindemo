import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.example.androidtest.R;
import com.fg.androidtext.sqlite.MyDatabaseHelper;
import com.fg.androidtext.sqlite.MyloginCursor;

public class LoginActivity extends Activity implements OnClickListener {

	private EditText loginId;
	private EditText loginPassword;
	private Button loginBtn;
	private Button loginMissps;
	private Button loginNewUser;
	private Button loginChangePw;
	private String isId, isPs;
	private SQLiteOpenHelper helper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_login);
		helper = new MyDatabaseHelper(this);
		initView();
		Intent i = super.getIntent();
		String Id = i.getStringExtra("myId");
		loginId.setText(Id);
	}

	// 控件的初始化
	private void initView() {
		loginId = (EditText) findViewById(R.id.loginId);
		loginPassword = (EditText) findViewById(R.id.loginPassword);
		loginBtn = (Button) findViewById(R.id.loginBtn);
		loginBtn.setOnClickListener(this);
		loginMissps = (Button) findViewById(R.id.loginMissps);
		loginMissps.setOnClickListener(this);
		loginNewUser = (Button) findViewById(R.id.loginNewUser);
		loginNewUser.setOnClickListener(this);
		loginChangePw = (Button) findViewById(R.id.loginChangePw);
		loginChangePw.setOnClickListener(this);
	}

	// 控件的点击事件
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.loginBtn:
			isId = loginId.getText().toString();
			isPs = loginPassword.getText().toString();
			if (new MyloginCursor(
					LoginActivity.this.helper.getReadableDatabase()).find(isId)
					.size() == 0) {
				// Toast弹窗
				Toast.makeText(LoginActivity.this, "手机号未注册，请注册后登录",
						Toast.LENGTH_SHORT).show();
			} else {
				String lph = new MyloginCursor(
						LoginActivity.this.helper.getReadableDatabase()).find(
						isId).toString();
				// 对查询出来的数据进行拆分
				String result[] = lph.split(",");
				if (result[1].equals(isId) && result[2].equals(isPs)) {
					Toast.makeText(LoginActivity.this, "登录成功",
							Toast.LENGTH_SHORT).show();
				} else {
					Toast.makeText(LoginActivity.this, "用户名或密码错误",
							Toast.LENGTH_SHORT).show();
				}
			}
			break;
		case R.id.loginMissps:
			Intent a = new Intent(LoginActivity.this,
					FindPasswordActivity.class);
			startActivity(a);
			break;
		case R.id.loginNewUser:
			Intent i = new Intent(LoginActivity.this, RegisterActivity.class);
			startActivity(i);
			break;
		case R.id.loginChangePw:
			Intent l = new Intent(LoginActivity.this,
					ChangePasswordActivity.class);
			startActivity(l);
			break;
		default:
			break;
		}

	}
}
